package com.example.organizer.model;

import java.io.Serializable;
import java.util.List;

import com.example.organizer.model.Task;

//Grupa
public class Group implements Serializable {
	
	public static final String GROUP_BUNDLE_KEY = "group_bundle_key";  

	private String id;

	private String title;

	private List<Task> tasksList;
	
	public Group(){
		
	}

	public Group(String id, String title, List<Task> tasksList) {
		super();
		this.id = id;
		this.title = title;
		this.tasksList = tasksList;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Task> getTasksList() {
		return tasksList;
	}

	public void setTasksList(List<Task> tasksList) {
		this.tasksList = tasksList;
	}
	
}
