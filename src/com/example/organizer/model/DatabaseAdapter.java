package com.example.organizer.model;

import java.util.Calendar;
import java.util.UUID;

import com.example.organizer.model.Task;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

//Baza danych
public class DatabaseAdapter {

	//Do logowania
	public static final String TAG = "organizer";

	private DatabaseHelper databaseHelper;
	private SQLiteDatabase sqLiteDatabase;
	
	//Obecna aktywnosc
	private final Context context;
	
	//Nazwa bazy
	public static final String DATABASE_NAME = "organizer_task_management.db";

	public static final int DATABASE_VERSION = 2;

	public static final String GROUP_TABLE_NAME = "_group";
	public static final String GROUP_TABLE_COLUMN_ID = "_id";
	public static final String GROUP_TABLE_COULMN_TITLE = "_title";
	private static final String GROUP_TABLE_CREATE
			= "create table " + GROUP_TABLE_NAME
			+ " ( "
			+ GROUP_TABLE_COLUMN_ID + " text primary key, "
			+ GROUP_TABLE_COULMN_TITLE + " text not null "
			+ " );";
	
	public static final String TASK_TABLE_NAME = "_task";
	public static final String TASK_TABLE_COLUMN_ID = "_id";
	public static final String TASK_TABLE_COLUMN_TITLE = "_title";
	public static final String TASK_TABLE_COLUMN_DUE_DATE = "_due_date";
	public static final String TASK_TABLE_COLUMN_NOTE = "_note";
	public static final String TASK_TABLE_COLUMN_PRIORITY = "_priority";
	public static final String TASK_TABLE_COLUMN_GROUP = "_group";
	public static final String TASK_TABLE_COLUMN_COMPLETION_STATUS = "_completion_status";
	public static final String TASK_TABLE_CREATE
			= "create table " + TASK_TABLE_NAME
			+ " ( "
			+ TASK_TABLE_COLUMN_ID + " text primary key, "
			+ TASK_TABLE_COLUMN_TITLE + " text not null, "
			+ TASK_TABLE_COLUMN_DUE_DATE + " integer not null, "
			+ TASK_TABLE_COLUMN_NOTE + " text,"
			+ TASK_TABLE_COLUMN_PRIORITY + " integer not null, "
			+ TASK_TABLE_COLUMN_GROUP + " text not null, "
			+ TASK_TABLE_COLUMN_COMPLETION_STATUS + " integer not null, "
			+ "foreign key ( " + TASK_TABLE_COLUMN_GROUP + " ) references " + GROUP_TABLE_NAME + " ( " + GROUP_TABLE_COLUMN_ID + " ) "
			+ " );";
			
	
	//Tablica wspolpracownikow
	public static final String COLLABORATOR_TABLE_NAME = "_collaborator";
	public static final String COLLABORATOR_TABLE_COLUMN_ID = "_id";
	public static final String COLLABORATOR_TABLE_COLUMN_EMAIL = "_email";
	public static final String COLLABORATOR_TABLE_COLUMN_TASK_ID = "_task_id";
	public static final String COLLABORATOR_TABLE_CREATE
			= "create table " + COLLABORATOR_TABLE_NAME
			+ " ( "
			+ COLLABORATOR_TABLE_COLUMN_ID + " integer primary key autoincrement, "
			+ COLLABORATOR_TABLE_COLUMN_EMAIL + " text not null, "
			+ COLLABORATOR_TABLE_COLUMN_TASK_ID + " text not null, "
			+ "foreign key ( " + COLLABORATOR_TABLE_COLUMN_TASK_ID + " ) references " + TASK_TABLE_NAME + " ( " + TASK_TABLE_COLUMN_ID + " ) "
			+ " );";
	
	private static class DatabaseHelper extends SQLiteOpenHelper{

		public DatabaseHelper(Context context, String name, CursorFactory factory, int version){
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DatabaseAdapter.GROUP_TABLE_CREATE);
			db.execSQL(DatabaseAdapter.TASK_TABLE_CREATE);
			db.execSQL(DatabaseAdapter.COLLABORATOR_TABLE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("Drop table if exists " + DatabaseAdapter.COLLABORATOR_TABLE_NAME);
			db.execSQL("Drop table if exists " + DatabaseAdapter.TASK_TABLE_NAME);
			db.execSQL("Drop table if exists " + DatabaseAdapter.GROUP_TABLE_NAME);
			onCreate(db);
		}
		
	}
	
	public DatabaseAdapter(Context context){
		this.context = context;
	}

	//Nawiazywanie polaczenia z baza
	public DatabaseAdapter open(){
		databaseHelper = new DatabaseHelper(context, this.DATABASE_NAME, null, this.DATABASE_VERSION);
		sqLiteDatabase = databaseHelper.getWritableDatabase();
		return this;
	}

	public void close(){
		databaseHelper.close();
	}

	public Cursor getAllGroups(){
		return sqLiteDatabase.query(GROUP_TABLE_NAME,
				new String[] {GROUP_TABLE_COLUMN_ID, GROUP_TABLE_COULMN_TITLE}, null, null, null, null, null);
	}

	public Cursor getGroupById(String groupId){
		return sqLiteDatabase.query(GROUP_TABLE_NAME,
				new String[] {GROUP_TABLE_COLUMN_ID, GROUP_TABLE_COULMN_TITLE},
				GROUP_TABLE_COLUMN_ID + " = '" + groupId + "'", null, null, null, null);
	}

	public long insertGroup(String groupID, String groupTitle){
		ContentValues initialValues = new ContentValues();
		initialValues.put(GROUP_TABLE_COLUMN_ID, groupID);
		initialValues.put(GROUP_TABLE_COULMN_TITLE, groupTitle);
		return sqLiteDatabase.insert(GROUP_TABLE_NAME, null, initialValues);
	}

	public void insertTask(Task task){
		ContentValues initialValues = new ContentValues();
		initialValues.put(TASK_TABLE_COLUMN_ID, task.getId());
		initialValues.put(TASK_TABLE_COLUMN_TITLE, task.getTitle());
		initialValues.put(TASK_TABLE_COLUMN_DUE_DATE, task.getDueDate().getTimeInMillis());
		initialValues.put(TASK_TABLE_COLUMN_NOTE, task.getNote());
		initialValues.put(TASK_TABLE_COLUMN_PRIORITY, task.getPriorityLevel());
		initialValues.put(TASK_TABLE_COLUMN_GROUP, task.getGroup().getId());
		initialValues.put(TASK_TABLE_COLUMN_COMPLETION_STATUS, task.getCompletionStatus());
		sqLiteDatabase.insert(TASK_TABLE_NAME, null, initialValues);

		this.insertCollaborators(task);
	}

	public Cursor getAllTasks(){
		return sqLiteDatabase.query(TASK_TABLE_NAME,
				new String[] {TASK_TABLE_COLUMN_ID, TASK_TABLE_COLUMN_TITLE, TASK_TABLE_COLUMN_DUE_DATE, TASK_TABLE_COLUMN_NOTE, TASK_TABLE_COLUMN_PRIORITY, TASK_TABLE_COLUMN_GROUP, TASK_TABLE_COLUMN_COMPLETION_STATUS},
				null, null, null, null, null);
	}

	public Cursor getTaskById(String taskId){
		return sqLiteDatabase.query(TASK_TABLE_NAME,
				new String[] {TASK_TABLE_COLUMN_ID, TASK_TABLE_COLUMN_TITLE, TASK_TABLE_COLUMN_DUE_DATE, TASK_TABLE_COLUMN_NOTE, TASK_TABLE_COLUMN_PRIORITY, TASK_TABLE_COLUMN_GROUP, TASK_TABLE_COLUMN_COMPLETION_STATUS},
				TASK_TABLE_COLUMN_ID + " = '" + taskId + "'", null, null, null, null);
	}

	public Cursor getCollaboratorsByTaskId(String taskId){
		return sqLiteDatabase.query(COLLABORATOR_TABLE_NAME,
				new String[] {COLLABORATOR_TABLE_COLUMN_ID, COLLABORATOR_TABLE_COLUMN_EMAIL, COLLABORATOR_TABLE_COLUMN_TASK_ID},
				COLLABORATOR_TABLE_COLUMN_TASK_ID + " = '" + taskId + "'", null, null, null, null);
	}

	public void insertCollaborators(Task task){
		for (String email : task.getCollaborators()){
			ContentValues initialValues = new ContentValues();
			initialValues.put(COLLABORATOR_TABLE_COLUMN_EMAIL, email);
			initialValues.put(COLLABORATOR_TABLE_COLUMN_TASK_ID, task.getId());
			sqLiteDatabase.insert(COLLABORATOR_TABLE_NAME, null, initialValues);
		}
	}
	
	//Edycja zadania
	public void editExistingTask(Task task){
		ContentValues updateValues;

		updateValues = new ContentValues();
		updateValues.put(TASK_TABLE_COLUMN_TITLE, task.getTitle());
		updateValues.put(TASK_TABLE_COLUMN_NOTE, task.getNote());
		updateValues.put(TASK_TABLE_COLUMN_DUE_DATE, task.getDueDate().getTimeInMillis());
		updateValues.put(TASK_TABLE_COLUMN_PRIORITY, task.getPriorityLevel());
		updateValues.put(TASK_TABLE_COLUMN_GROUP, task.getGroup().getId());
		updateValues.put(TASK_TABLE_COLUMN_COMPLETION_STATUS, task.getCompletionStatus());
		sqLiteDatabase.update(TASK_TABLE_NAME, updateValues, TASK_TABLE_COLUMN_ID + " = '" + task.getId() + "'", null);

	}

	public void deleteTask(Task task){
		deleteTask(task.getId());
	}

	public void deleteTask(String taskId){
		sqLiteDatabase.delete(TASK_TABLE_NAME, TASK_TABLE_COLUMN_ID + " = '" + taskId + "'", null);
	}

	public String getNewTaskId(){
		String uuid = null;
		Cursor cursor = null;

		do {
			uuid = UUID.randomUUID().toString();
			cursor = getTaskById(uuid);
		} while (cursor.getCount() > 0);
		
		return uuid;
	}

	public String getNewGroupId(){
		String uuid = null;
		Cursor cursor = null;
	
		do {
			uuid = UUID.randomUUID().toString();
			cursor = getGroupById(uuid);
		} while (cursor.getCount() > 0);
		
		return uuid;
	}
}
