package com.example.organizer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

//Zadania
public class Task implements Serializable {
	
	public static final int HIGH_PRIORITY = 0;
	public static final int MEDIUM_PRIORITY = 1;
	public static final int LOW_PRIORITY = 2;

	public static final int TASK_COMPLETED = 0;
	public static final int TASK_NOT_COMPLETED = 1;

	public static final String TASK_BUNDLE_KEY = "task_bundle_key";  

	private String id;

	private String title;

	private Calendar dueDate;

	private String note;
	
	private int priorityLevel;

	private List<String> collaborators;

	private Group group;
	
	private int completionStatus;
	
	public Task(){
		this.collaborators = new ArrayList<String>();
		this.id = "";
		this.title = "";
		this.dueDate = Calendar.getInstance();
		this.note = "";
		this.priorityLevel = HIGH_PRIORITY;
		this.group = new Group();
		this.completionStatus = TASK_COMPLETED;
	}
	
	public Task(String id, String title, Calendar dueDate, String note,
			int priorityLevel, List<String> collaborators, Group group,
			int completionStatus) {
		super();
		this.id = id;
		this.title = title;
		this.dueDate = dueDate;
		this.note = note;
		this.priorityLevel = priorityLevel;
		this.collaborators = collaborators;
		this.group = group;
		this.completionStatus = completionStatus;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Calendar getDueDate() {
		return dueDate;
	}

	public void setDueDate(Calendar dueDate) {
		this.dueDate = dueDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public int getPriorityLevel() {
		return priorityLevel;
	}

	public void setPriorityLevel(int priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

	public List<String> getCollaborators() {
		return collaborators;
	}

	public void setCollaborators(List<String> collaborators) {
		this.collaborators = collaborators;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public int getCompletionStatus() {
		return completionStatus;
	}

	public void setCompletionStatus(int completionStatus) {
		this.completionStatus = completionStatus;
	}
	
}
