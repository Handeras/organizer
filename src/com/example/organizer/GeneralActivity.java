package com.example.organizer;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;

//Ta klasa jest dla cel�w wielokrotnego u�ytku
//Ta klasa ustali� pewne konfiguracje domy�lne dla wszystkich aplikacji
//Wszystkie dzia�ania w tej Aplikacji, z wyj�tkiem MainActivity rozszerza 
//t� klas� tak, �e aplikacja mo�e zachowa� ten sam uk�ad wy�wietlacza

public class GeneralActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// w��czy� ikon� do nawigacji
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Przycisk do domu zestaw jak przycisk wstecz
		switch (item.getItemId()){
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	
	
}
