package com.example.organizer;

import com.example.organizer.R;
import com.example.organizer.R.id;
import com.example.organizer.R.layout;
import com.example.organizer.R.menu;
import com.example.organizer.controller.ApplicationNavigationHandler;
import com.example.organizer.model.DatabaseAdapter;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class ViewAllGroupsActivity extends GeneralActivity {
	
	//Baza danych
	private DatabaseAdapter databaseAdapter;
	
	//kursor do grup
	private Cursor allGroupsCursor;
	
	private SimpleCursorAdapter allGroupsListViewAdapter;

	private ListView allGroupsListView;

	public static final int ADD_NEW_GROUP_REQUEST_CODE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_all_groups);
		
		//Pobranie wszystkich grup
		this.allGroupsListView = (ListView) findViewById(R.id.activity_view_all_groups_Listview_all_groups);

		//Polaczenie z baza
		databaseAdapter = new DatabaseAdapter(this);
		databaseAdapter.open();
		//Pobranie danych z bazy
		initAllGroupsListView();
		
	}
	
	//Pobranie grup i zaladowanie do ListView
	private void initAllGroupsListView(){
		if(this.databaseAdapter != null){
			allGroupsCursor = databaseAdapter.getAllGroups();
			startManagingCursor(allGroupsCursor);
			String[] from = new String[]{DatabaseAdapter.GROUP_TABLE_COULMN_TITLE};
			int[] to = new int[]{R.id.activity_view_all_groups_listview_all_groups_layout_textview_group_title};
			allGroupsListViewAdapter = new SimpleCursorAdapter(this,
					R.layout.activity_view_all_groups_listview_all_groups_layout, allGroupsCursor, from, to);
			this.allGroupsListView.setAdapter(allGroupsListViewAdapter);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.view_all_groups, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
		case R.id.activity_view_all_groups_Menu_actionbar_Item_add_group:
			ApplicationNavigationHandler.addNewGroup(this, ViewAllGroupsActivity.ADD_NEW_GROUP_REQUEST_CODE);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		//Sprawdzanie czy jest dodanie nowej grupy
		if(requestCode == ViewAllGroupsActivity.ADD_NEW_GROUP_REQUEST_CODE){
			initAllGroupsListView();
		}
	}

}
