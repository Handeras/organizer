package com.example.organizer;

import java.util.ArrayList;
import java.util.Calendar;

import com.example.organizer.R;
import com.example.organizer.R.layout;
import com.example.organizer.R.menu;
import com.example.organizer.controller.ConfirmCancelDialogHandler;
import com.example.organizer.model.DatabaseAdapter;
import com.example.organizer.model.Group;
import com.example.organizer.model.Task;
import com.example.organizer.ViewTaskDetailActivity;

import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

//Aktywno�� ta jest u�ywana do 2 cel�w: Tworzenie nowego zadania i edycji istniej�cego zadania
//Za ka�dym razem gdy jest za�adowana, program powinien sprawdzi�, czy obiekt w pakiecie zada� istnieje
//Je�li istnieje, oznacza to, �e aktywno�� b�dzie edytowa� to zadanie
//W przeciwnym razie aktywno�� zamierza stworzy� nowe zadanie

public class ModifyTaskActivity extends GeneralActivity {

	// Tworzenie nowego zadania lub pobieranie ju� istniej�cego
	private Task task = null;

	// Edycja lub dodanie zadania
	private int currentJob;
	private final int CURRENT_JOB_EDIT = 1;
	private final int CURRENT_JOB_ADD = 2;

	public static final int SELECT_COLLABORATOR_ACTIVITY_RESULT_CODE = 1;

	private ArrayAdapter<String> collaboratorsListViewAdapter;

	private DatabaseAdapter databaseAdapter;

	private Cursor allGroupsCursor;

	private SimpleCursorAdapter allGroupsSpinnerAdapter;

	private Spinner allGroupsSpinner;


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent resultIntent;
		Bundle resultBundle;

		switch (item.getItemId()){

		//Anuluj/Home - dialog
		case R.id.activity_modify_task_Menu_actionbar_Item_cancel:
		case android.R.id.home:
			//ustawienie rezultatow poprzedniej aktywnosci
			resultIntent = new Intent();
			resultBundle = new Bundle();
			resultBundle.putSerializable(Task.TASK_BUNDLE_KEY, this.task);
			resultIntent.putExtras(resultBundle);
			setResult(ViewTaskDetailActivity.EDIT_TASK_REQUEST_CODE, resultIntent);

			//Dialog
			ConfirmCancelDialogHandler.showConfirmCancelDialog(this);
			return true;

			// Zapis
		case R.id.activity_modify_task_Menu_actionbar_Item_save:

			//Sprawdzanie czy edycja czy dodanie nowego
			if(this.currentJob == CURRENT_JOB_ADD){
				//nowy
				addNewTask();
			} else {
				//edycja
				editExistingTask();

				resultIntent = new Intent();
				resultBundle = new Bundle();
				resultBundle.putSerializable(Task.TASK_BUNDLE_KEY, this.task);
				resultIntent.putExtras(resultBundle);
				setResult(ViewTaskDetailActivity.EDIT_TASK_REQUEST_CODE, resultIntent);

			}

			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onBackPressed() {
		//powrot
		Intent resultIntent = new Intent();
		Bundle resultBundle = new Bundle();
		resultBundle.putSerializable(Task.TASK_BUNDLE_KEY, this.task);
		resultIntent.putExtras(resultBundle);
		setResult(ViewTaskDetailActivity.EDIT_TASK_REQUEST_CODE, resultIntent);

		ConfirmCancelDialogHandler.showConfirmCancelDialog(this);
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_modify_task);

		allGroupsSpinner = (Spinner) findViewById(R.id.activity_modify_task_Spinner_group);

		//Polaczenie z baza
		databaseAdapter = new DatabaseAdapter(this);
		databaseAdapter.open();

		//inicjalizacja grupy
		this.initGroup();

		//Sprawdzanie czy tworzenie czy edycja
		Bundle modifyTaskBundle = this.getIntent().getExtras();
		try {
			this.task = (Task) modifyTaskBundle.getSerializable(Task.TASK_BUNDLE_KEY);
		} catch (Exception ex){
			ex.printStackTrace();
		}
		if (task != null){
			this.currentJob = this.CURRENT_JOB_EDIT;
			putDataToForm();
		} else {
			this.task = new Task();
			this.currentJob = this.CURRENT_JOB_ADD;
		}

		this.initColaborators();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == ModifyTaskActivity.SELECT_COLLABORATOR_ACTIVITY_RESULT_CODE){
			//pobranie maila
			if(data != null){
				String collaboratorEmailAddress = getCollaboratorEmailAddress(data);
				this.task.getCollaborators().add(collaboratorEmailAddress);
				this.collaboratorsListViewAdapter.notifyDataSetChanged();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//dodanie zadania
		getMenuInflater().inflate(R.menu.modify_task, menu);
		return true;
	}

	//edycja
	private void editExistingTask(){
		updateTaskObject();
		databaseAdapter.editExistingTask(this.task);
	}

	//Pobranie zadania i jego edycja
	private void updateTaskObject(){
		String taskTitle = ((EditText)findViewById(R.id.activity_modify_task_Edittext_task_title)).getText().toString();
		this.task.setTitle(taskTitle);
		//dodanie daty
		DatePicker taskDueDatePicker = (DatePicker) findViewById(R.id.activity_modify_task_Datepicker_due_date);
		this.task.getDueDate().set(Calendar.DATE, taskDueDatePicker.getDayOfMonth());
		this.task.getDueDate().set(Calendar.MONTH, taskDueDatePicker.getMonth());
		this.task.getDueDate().set(Calendar.YEAR, taskDueDatePicker.getYear());
		//dodanie notki
		String taskNote = ((EditText)findViewById(R.id.activity_modify_task_EditText_note)).getText().toString();
		this.task.setNote(taskNote);
		//priorytet
		int priorityLevel = ((Spinner)findViewById(R.id.activity_modify_task_Spinner_priority_level)).getSelectedItemPosition();
		this.task.setPriorityLevel(priorityLevel);
		//status
		int completionStatus = ((Spinner)findViewById(R.id.activity_modify_task_Spinner_completion_status)).getSelectedItemPosition();
		this.task.setCompletionStatus(completionStatus);
		//grupa
		Spinner groupSpinner = (Spinner) findViewById(R.id.activity_modify_task_Spinner_group);
		this.task.getGroup().setId(getGroupIdByPosition(allGroupsCursor, groupSpinner.getSelectedItemPosition()));
	}

	//Dodanie nowego zadania do bazy
	private void addNewTask(){
		updateTaskObject();
		//ustawienie id zadania
		String taskId = databaseAdapter.getNewTaskId();
		this.task.setId(taskId);
		this.databaseAdapter.insertTask(this.task);
	}

	//laduje wszystkie grupy
	private void initGroup(){
		//sprawdza czy baza nie ma null
		if(this.databaseAdapter != null){
			//pobranie wszystkich grup
			allGroupsCursor = databaseAdapter.getAllGroups();
			startManagingCursor(allGroupsCursor);
			String[] from = new String[]{DatabaseAdapter.GROUP_TABLE_COULMN_TITLE};
			int[] to = new int[]{R.id.activity_view_all_groups_listview_all_groups_layout_textview_group_title};
			this.allGroupsSpinnerAdapter = new SimpleCursorAdapter(this,
					R.layout.activity_view_all_groups_listview_all_groups_layout, allGroupsCursor, from, to);
			allGroupsSpinner.setAdapter(allGroupsSpinnerAdapter);
		} else {
			finish();
		}
	}

	//Inicjalizacja wspolpracownikow
	private void initColaborators(){
		collaboratorsListViewAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, this.task.getCollaborators());
		ListView collaboratorsListView = (ListView) findViewById(R.id.activity_modify_task_ListView_collaborators);
		collaboratorsListView.setAdapter(collaboratorsListViewAdapter);

		Button selectCollaboratorButton = (Button) findViewById(R.id.activity_modify_task_Button_select_collaborator);
		selectCollaboratorButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				selectCollaboratorEmail();
			}
		});

		Button clearCollaboratorsButton = (Button) findViewById(R.id.activity_modify_task_Button_clear_collaborator);
		clearCollaboratorsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				clearCollaboratorsList();
			}
		});
	}

	//usuwanie wspolpracownikow
	private void clearCollaboratorsList(){
		this.task.getCollaborators().clear();
		this.collaboratorsListViewAdapter.notifyDataSetChanged();
	}

	private void selectCollaboratorEmail(){
		Intent selectCollaboratorIntent = new Intent(Intent.ACTION_PICK);
		selectCollaboratorIntent.setType(ContactsContract.CommonDataKinds.Email.CONTENT_TYPE);
		startActivityForResult(selectCollaboratorIntent,
				ModifyTaskActivity.SELECT_COLLABORATOR_ACTIVITY_RESULT_CODE);
	}

	//Pobieranie maili wspolpracownikow
	private String getCollaboratorEmailAddress(Intent data){
		String collaboratorEmailAddress = null;		
		Uri uri = data.getData();

		if (uri != null) {
			Cursor c = null;
			try {
				c = getContentResolver().query(uri, new String[]{ 
						ContactsContract.CommonDataKinds.Email.ADDRESS,  
						ContactsContract.CommonDataKinds.Email.TYPE },
						null, null, null);

				if (c != null && c.moveToFirst()) {
					collaboratorEmailAddress = c.getString(0);
				}
			} finally {
				if (c != null) {
					c.close();
				}
			}
		}

		return collaboratorEmailAddress;
	}

	private void putDataToForm(){
		if (this.currentJob == this.CURRENT_JOB_EDIT){
			EditText taskTitleEditText = (EditText) findViewById(R.id.activity_modify_task_Edittext_task_title);
			taskTitleEditText.setText(this.task.getTitle());

			DatePicker taskDueDatePicker = (DatePicker) findViewById(R.id.activity_modify_task_Datepicker_due_date);
			taskDueDatePicker.updateDate(this.task.getDueDate().get(Calendar.YEAR),
					this.task.getDueDate().get(Calendar.MONTH),
					this.task.getDueDate().get(Calendar.DATE));

			EditText taskNoteEditText = (EditText) findViewById(R.id.activity_modify_task_EditText_note);
			taskNoteEditText.setText(this.task.getNote());

			Spinner taskPriorityLevelSpinner = (Spinner) findViewById(R.id.activity_modify_task_Spinner_priority_level);
			taskPriorityLevelSpinner.setSelection(this.task.getPriorityLevel());

			allGroupsSpinner.setSelection(this.getGroupPositionInCursor(allGroupsCursor, this.task.getGroup().getId()));

			Spinner completionStatusSpinner = (Spinner) findViewById(R.id.activity_modify_task_Spinner_completion_status);
			completionStatusSpinner.setSelection(this.task.getCompletionStatus());
		}
	}

	private String getGroupIdByPosition(Cursor cursor, int position){
		String groupId = null;
		cursor.moveToFirst();
		cursor.move(position);
		groupId = cursor.getString(cursor.getColumnIndex(DatabaseAdapter.GROUP_TABLE_COLUMN_ID));
		return groupId;
	}

	private int getGroupPositionInCursor(Cursor cursor, String groupId){
		int position = -1;
		cursor.moveToFirst();
		while(!cursor.isAfterLast()){
			String currentGroupId = cursor.getString(cursor.getColumnIndex(DatabaseAdapter.GROUP_TABLE_COLUMN_ID));
			if(currentGroupId.equals(groupId)){
				position = cursor.getPosition();
			}
			cursor.moveToNext();
		}
		return position;
	}

}
