package com.example.organizer;

import java.util.Calendar;
import java.util.Locale;

import com.example.organizer.R;
import com.example.organizer.R.layout;
import com.example.organizer.R.menu;
import com.example.organizer.controller.ApplicationNavigationHandler;
import com.example.organizer.controller.ConfirmDeletionDialog;
import com.example.organizer.model.DatabaseAdapter;
import com.example.organizer.model.Task;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ViewTaskDetailActivity extends GeneralActivity {

	//Wyswietlanie zadania
	private Task task;

	public static final int EDIT_TASK_REQUEST_CODE = 1;

	private DatabaseAdapter databaseAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_task_detail);

		databaseAdapter = new DatabaseAdapter(this);
		databaseAdapter.open();

		Bundle taskDetailBundle = this.getIntent().getExtras();
		try{
			this.task = (Task) taskDetailBundle.getSerializable(Task.TASK_BUNDLE_KEY);
		} catch (Exception ex){
			ex.printStackTrace();
		}

		if(this.task == null){
			this.finish();
		} else {
			this.putDataToView();
		}

	}

	private void putDataToView(){
		if(this.task == null){
			this.finish();
		} else {
			TextView taskTitleTextView = (TextView) findViewById(R.id.activity_view_task_detail_TextView_task_title_content);
			taskTitleTextView.setText(this.task.getTitle());
			TextView taskDueDateTextView = (TextView) findViewById(R.id.activity_view_task_detail_TextView_due_date_content);
			Calendar dueDate = this.task.getDueDate();
			String dueDateString = dueDate.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US) + " "
					+ dueDate.get(Calendar.DATE) + " "
					+ dueDate.get(Calendar.YEAR);
			taskDueDateTextView.setText(dueDateString);
			TextView taskNoteTextView = (TextView) findViewById(R.id.activity_view_task_detail_TextView_note_content);
			taskNoteTextView.setText(this.task.getNote());
			TextView priorityTextView = (TextView) findViewById(R.id.activity_view_task_detail_TextView_prority_level_content);
			String priorityString;
			switch (this.task.getPriorityLevel()){
			case Task.HIGH_PRIORITY:
				priorityString = this.getString(R.string.activity_modify_task_Spinner_priority_level_Item_high_String_title);
				break;
			case Task.MEDIUM_PRIORITY:
				priorityString = this.getString(R.string.activity_modify_task_Spinner_priority_level_Item_medium_String_title);
				break;
			default:
				priorityString = this.getString(R.string.activity_modify_task_Spinner_priority_level_Item_low_String_title);
				break;
			}
			priorityTextView.setText(priorityString);
			TextView completionTextView = (TextView) findViewById(R.id.activity_view_task_detail_TextView_status_content);
			String completionString;
			if(this.task.getCompletionStatus() == Task.TASK_COMPLETED){
				completionString = getString(R.string.activity_modify_task_Spinner_completion_status_Item_yes_String_title);
			} else {
				completionString = getString(R.string.activity_modify_task_Spinner_completion_status_Item_no_String_title);
			}
			completionTextView.setText(completionString);
			TextView groupTextView = (TextView) findViewById(R.id.activity_view_task_detail_TextView_group_content);
			groupTextView.setText(this.task.getGroup().getTitle());
			TextView collaboratorsTextView = (TextView) findViewById(R.id.activity_view_task_detail_TextView_collaborators_content);
			if(this.task.getCollaborators().isEmpty()){
				collaboratorsTextView.setText("Nie ma wsp�pracownik�w.");
			} else {
				String collaboratorString = "";
				for(String collaboratorEmail : this.task.getCollaborators()){
					collaboratorString = collaboratorString + collaboratorEmail + "\n";
				}
				collaboratorsTextView.setText(collaboratorString);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.view_task_detail, menu);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == EDIT_TASK_REQUEST_CODE){
			this.task = (Task) data.getExtras().getSerializable(Task.TASK_BUNDLE_KEY);
			this.putDataToView();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
		case R.id.activity_view_task_detail_Menu_actionbar_Item_edit:
			ApplicationNavigationHandler.editExistingTask(this, this.task);
			return true;
		case R.id.activity_view_task_detail_Menu_actionbar_Item_delete:
			ConfirmDeletionDialog.showConfirmDeleteDialogForTask(this, this.task, this.databaseAdapter);
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
