package com.example.organizer.controller;

import com.example.organizer.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

//Komunikacja o anulowanie
public class ConfirmCancelDialogHandler {

	public static void showConfirmCancelDialog(Activity sourceActivity){
		Dialog confirmCancelDialog;
		confirmCancelDialog = new AlertDialog.Builder(sourceActivity)
			.setIcon(android.R.drawable.ic_menu_help)
			.setTitle(R.string.activity_modify_task_Dialog_confirm_cancel_TextView_label_String_title)
			.setPositiveButton(R.string.activity_modify_task_Dialog_confirm_cancel_Button_positive_String_title,
					new ConfirmCancelDialogPositiveButtonListener(sourceActivity))
			.setNegativeButton(R.string.activity_modify_task_Dialog_confirm_cancel_Button_negative_String_title,
					new ConfirmCancelDialogNegativeButtonListener())
			.create();
		confirmCancelDialog.show();
	}
	
	private static class ConfirmCancelDialogPositiveButtonListener implements OnClickListener{

		private Activity sourceActivity;

		public ConfirmCancelDialogPositiveButtonListener(Activity sourceActivity){
			this.sourceActivity = sourceActivity;
		}
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			sourceActivity.finish();
		}
		
	}

	private static class ConfirmCancelDialogNegativeButtonListener implements OnClickListener{

		@Override
		public void onClick(DialogInterface dialog, int which) {
			dialog.dismiss();
		}
		
	}
}
