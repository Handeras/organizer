package com.example.organizer.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

//Komunikaty z uzytkownikiem
public class MessageDialogHandler {

	//Statyczne funkcje do dialogow z uzytkownikiem
	public static void showMessageDialog(Activity sourceActivity, String message){
		Dialog messageDialog;
		messageDialog = new AlertDialog.Builder(sourceActivity)
			.setIcon(android.R.drawable.ic_menu_info_details)
			.setTitle(message)
			.setPositiveButton("OK", new MessageDialogPositiveButtonListener())
			.create();
		messageDialog.show();
	}

	private static class MessageDialogPositiveButtonListener implements OnClickListener {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			dialog.dismiss();
		}

	}
}
