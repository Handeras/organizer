package com.example.organizer.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.example.organizer.R;
import com.example.organizer.model.DatabaseAdapter;
import com.example.organizer.model.Task;

//Komunikaty o usunieciu
public class ConfirmDeletionDialog {

	public static void showConfirmDeleteDialogForTask(Activity sourceActivity, Task task, DatabaseAdapter databaseAdapter){
		Dialog confirmCancelDialog;
		confirmCancelDialog = new AlertDialog.Builder(sourceActivity)
		.setIcon(android.R.drawable.ic_menu_help)
		.setTitle("Jeste� pewny aby usun�� przedmiot?")
		.setPositiveButton("Tak",
				new ConfirmDeleteDialogForTaskPositiveButtonListener(sourceActivity, task, databaseAdapter))
		.setNegativeButton("Nie",
				new ConfirmDeleteDialogNegativeButtonListener())
		.create();
		confirmCancelDialog.show();
	}

	private static class ConfirmDeleteDialogForTaskPositiveButtonListener implements OnClickListener{

		private Activity sourceActivity;

		private Task task;

		private DatabaseAdapter databaseAdapter;

		public ConfirmDeleteDialogForTaskPositiveButtonListener(Activity sourceActivity, Task task, DatabaseAdapter databaseAdapter){
			this.sourceActivity = sourceActivity;
			this.task = task;
			this.databaseAdapter = databaseAdapter;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {
			databaseAdapter.deleteTask(task);

			sourceActivity.finish();
		}

	}

	private static class ConfirmDeleteDialogNegativeButtonListener implements OnClickListener{

		@Override
		public void onClick(DialogInterface dialog, int which) {
			dialog.dismiss();
		}

	}
}
