package com.example.organizer.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.example.organizer.controller.MessageDialogHandler;
import com.example.organizer.model.DatabaseAdapter;
import com.example.organizer.model.Group;
import com.example.organizer.model.Task;
import com.example.organizer.ModifyGroupActivity;
import com.example.organizer.ModifyTaskActivity;
import com.example.organizer.SettingActivity;
import com.example.organizer.ViewAllGroupsActivity;
import com.example.organizer.ViewAllTasksActivity;
import com.example.organizer.ViewTaskDetailActivity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Note;

//Statyczne funkcje do nawigacji po aplikacji
public class ApplicationNavigationHandler {
	
	//Przeglad wszystkich grup
	public static void showAllGroups(Activity sourceActivity){
		Intent showAllGroupsIntent = new Intent(sourceActivity, ViewAllGroupsActivity.class);
		sourceActivity.startActivity(showAllGroupsIntent);
	}
	
	//Przeglad zadania
	public static void viewTaskDetail(Activity sourceActivity, Task task){
		Intent viewTaskDetailIntent = new Intent(sourceActivity, ViewTaskDetailActivity.class);
		Bundle viewTaskDetailBundle = new Bundle();
		viewTaskDetailBundle.putSerializable(Task.TASK_BUNDLE_KEY, task);
		viewTaskDetailIntent.putExtras(viewTaskDetailBundle);
		sourceActivity.startActivity(viewTaskDetailIntent);
	}
	
	//Przeglad wszystkich zadan
	public static void showAllTasks(Activity sourceActivity){
		Intent showAllTasksIntent = new Intent(sourceActivity, ViewAllTasksActivity.class);
		sourceActivity.startActivity(showAllTasksIntent);
	}

	public static void showSetting(Activity sourceActivity){
		Intent showSettingIntent = new Intent(sourceActivity, SettingActivity.class);
		sourceActivity.startActivity(showSettingIntent);
	}
	
	//Dodawanie nowej grupy
	public static void addNewGroup(Activity sourceActivity, int resultCode){
		Intent addNewGroupIntent = new Intent(sourceActivity, ModifyGroupActivity.class);
		sourceActivity.startActivityForResult(addNewGroupIntent, resultCode);
	}
	
	//Edycja istniejacej grupy
	public static void editExistingGroup(Activity sourceActivity, Group existingGroup){
		Intent editExistingGroupIntent = new Intent(sourceActivity, ModifyGroupActivity.class);
		Bundle editExistingGroupBundle = new Bundle();
		editExistingGroupBundle.putSerializable(Group.GROUP_BUNDLE_KEY, existingGroup);
		editExistingGroupIntent.putExtras(editExistingGroupBundle);
		sourceActivity.startActivity(editExistingGroupIntent);
	}
	
	//Edycja zadania
	public static void editExistingTask(Activity sourceActivity, Task existingTask){
		Intent editExistingTaskIntent = new Intent(sourceActivity, ModifyTaskActivity.class);
		Bundle editExistingTaskBundle = new Bundle();
		editExistingTaskBundle.putSerializable(Task.TASK_BUNDLE_KEY, existingTask);
		editExistingTaskIntent.putExtras(editExistingTaskBundle);
		sourceActivity.startActivityForResult(editExistingTaskIntent, ViewTaskDetailActivity.EDIT_TASK_REQUEST_CODE);
	}
	
	//Dodanie nowego zadania
	public static void addNewTask(Activity sourceActivity, DatabaseAdapter databaseAdapter){
		Cursor allGroupsCursor = databaseAdapter.getAllGroups();
		if(allGroupsCursor.getCount() == 0){
			MessageDialogHandler.showMessageDialog(sourceActivity, "Nie ma �adnej grupy!\nNajpierw dodaj grupe.");
		} else {
			Intent addNewTaskIntent = new Intent(sourceActivity, ModifyTaskActivity.class);
			sourceActivity.startActivityForResult(addNewTaskIntent, ViewAllTasksActivity.ADD_NEW_TASK_REQUEST_CODE);
		}
	}
}
