package com.example.organizer;

import com.example.organizer.R;
import com.example.organizer.R.layout;
import com.example.organizer.R.menu;
import com.example.organizer.controller.ConfirmCancelDialogHandler;
import com.example.organizer.model.DatabaseAdapter;
import com.example.organizer.model.Group;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

//Aktywno�� ta jest u�ywana do 2 cel�w: Tworzenie nowej grupy i edycji grupy
//Za ka�dym razem gdy jest za�adowana, program powinien sprawdzi�, czy obiekt Grupa istnieje
//Je�li istnieje, oznacza to, �e aktywno�� b�dzie edytowa� t� grup�
//W przeciwnym razie aktywno�� zamierza stworzy� now� grup�

public class ModifyGroupActivity extends GeneralActivity {

	//obiekt Group do przechowywania danych aktualnie przetwarzanej grupy
	//je�li aktywno�� tworzy now� grup�, ten objekt jest null
	//w przeciwnym razie, to jest pobierane ze stosu
	private Group group = null;

	// DatabaseAdapter do interakcji z baza
	private DatabaseAdapter databaseAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_modify_group);

		//Sprawdza czy aktywno�� b�dzie tworzy� now� grup� lub edytowa� istniej�c�
		//Najpierw pobiera obiekt Group ze stosu
		Bundle modifyGroupBundle = this.getIntent().getExtras();
		try {
			this.group = (Group) modifyGroupBundle.getSerializable(Group.GROUP_BUNDLE_KEY);
		} catch (Exception ex){
			ex.printStackTrace();
		}
		// Nast�pnie sprawdza czy nie null
		if (group != null){
			//Je�li obiekt Group istnieje, �aduje go
			putDataToForm();
		} else {
		}

		//��czy si� z baz� danych
		databaseAdapter = new DatabaseAdapter(this);
		databaseAdapter.open();

	}

	// Pobiera dane o grupie
	private void putDataToForm(){
		//Najpierw sprawdza istnienie grupy
		if (group != null){
			//�adowanie nazwy grupy
			EditText groupTitleEditText = (EditText) findViewById(R.id.activity_modify_group_Edittext_group_title);
			groupTitleEditText.setText(this.group.getTitle());
		}
	}

	@Override
	public void onBackPressed() {
		//Pokazanie potwierdzenia dialogu
		ConfirmCancelDialogHandler.showConfirmCancelDialog(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){

		//Anuluj lub Home - potwierdzenie dialogu
		case R.id.activity_modify_group_Menu_actionbar_Item_cancel:
		case android.R.id.home:
			ConfirmCancelDialogHandler.showConfirmCancelDialog(this);
			return true;

			//Zapisanie danych do bazy
		case R.id.activity_modify_group_Menu_actionbar_Item_save:
			insertGroup();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	//"W�o�enie" obecnej grupy do bazy danych
	private void insertGroup(){
		//Pobranie nazwy
		EditText groupTitle = (EditText) findViewById(R.id.activity_modify_group_Edittext_group_title);
		//W�o�enie
		databaseAdapter.insertGroup(databaseAdapter.getNewGroupId(), groupTitle.getText().toString());
		//Zako�czenie tej aktywno�ci i powr�t do poprzedniej
		this.finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//Dodanie do menu
		getMenuInflater().inflate(R.menu.modify_group, menu);
		return true;
	}

}
