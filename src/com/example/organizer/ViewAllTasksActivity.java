package com.example.organizer;

import java.util.ArrayList;
import java.util.List;

import com.example.organizer.R;
import com.example.organizer.R.id;
import com.example.organizer.R.layout;
import com.example.organizer.R.menu;
import com.example.organizer.controller.ApplicationNavigationHandler;
import com.example.organizer.model.DatabaseAdapter;
import com.example.organizer.model.Group;
import com.example.organizer.model.Task;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.database.Cursor;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SimpleCursorAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ViewAllTasksActivity extends GeneralActivity {

	private ListView allTasksListView;

	private DatabaseAdapter databaseAdapter;

	private Cursor allTasksCursor;

	private SimpleCursorAdapter allTasksListViewAdapter;

	public static final int ADD_NEW_TASK_REQUEST_CODE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_all_tasks);

		allTasksListView = (ListView) findViewById(R.id.activity_view_all_tasks_Listview_all_tasks);
		allTasksListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				allTaskListViewItemClickHandler(arg0, arg1, arg2);
			}
		});

		//Polaczenie z baza
		databaseAdapter = new DatabaseAdapter(this);
		databaseAdapter.open();
		
		initAllTasksListView();
	}


	public void initAllTasksListView(){
		if(this.databaseAdapter != null){
			allTasksCursor = databaseAdapter.getAllTasks();
			startManagingCursor(allTasksCursor);
			String[] from = new String[]{DatabaseAdapter.TASK_TABLE_COLUMN_TITLE};
			int[] to = new int[]{R.id.activity_view_all_groups_listview_all_groups_layout_textview_group_title};
			allTasksListViewAdapter = new SimpleCursorAdapter(this,
					R.layout.activity_view_all_groups_listview_all_groups_layout, allTasksCursor, from, to);
			this.allTasksListView.setAdapter(allTasksListViewAdapter);
		}
	}

	//Obs�uga zadan
	private void allTaskListViewItemClickHandler(AdapterView<?> adapterView, View listView, int selectedItemId){
		//Dodanie nowego zadania
		Task selectedTask = new Task();
		allTasksCursor.moveToFirst();
		allTasksCursor.move(selectedItemId);
		selectedTask.setId(allTasksCursor.getString(allTasksCursor.getColumnIndex(DatabaseAdapter.TASK_TABLE_COLUMN_ID)));
		selectedTask.setTitle(allTasksCursor.getString(allTasksCursor.getColumnIndex(DatabaseAdapter.TASK_TABLE_COLUMN_TITLE)));
		selectedTask.getDueDate().setTimeInMillis(allTasksCursor.getLong(allTasksCursor.getColumnIndex(DatabaseAdapter.TASK_TABLE_COLUMN_DUE_DATE)));
		selectedTask.setNote(allTasksCursor.getString(allTasksCursor.getColumnIndex(DatabaseAdapter.TASK_TABLE_COLUMN_NOTE)));
		selectedTask.setPriorityLevel(allTasksCursor.getInt(allTasksCursor.getColumnIndex(DatabaseAdapter.TASK_TABLE_COLUMN_PRIORITY)));
		selectedTask.setCompletionStatus(allTasksCursor.getInt(allTasksCursor.getColumnIndex(DatabaseAdapter.TASK_TABLE_COLUMN_COMPLETION_STATUS)));
		selectedTask.setGroup(this.getGroupByTask(allTasksCursor.getString(allTasksCursor.getColumnIndex(DatabaseAdapter.TASK_TABLE_COLUMN_GROUP))));
		selectedTask.setCollaborators(this.getCollaboratorsListByTaskId(allTasksCursor.getString(allTasksCursor.getColumnIndex(DatabaseAdapter.TASK_TABLE_COLUMN_ID))));

		//Start aktywnosci
		ApplicationNavigationHandler.viewTaskDetail(this, selectedTask);
	}
	
	//Pobranie wspolpracownikow
	private List<String> getCollaboratorsListByTaskId(String taskId){
		List<String> collaboratorsList = new ArrayList<String>();
		
		Cursor collaboratorsCursor = databaseAdapter.getCollaboratorsByTaskId(taskId);
		startManagingCursor(collaboratorsCursor);
		collaboratorsCursor.moveToFirst();
		while(!collaboratorsCursor.isAfterLast()){
			String currentCollaboratorEmail = collaboratorsCursor.getString(collaboratorsCursor.getColumnIndex(DatabaseAdapter.COLLABORATOR_TABLE_COLUMN_EMAIL));
			collaboratorsList.add(currentCollaboratorEmail);
			collaboratorsCursor.moveToNext();
		}
		
		return collaboratorsList;
	}
	
	//Pobieranie zadan po id
	private Group getGroupByTask(String groupId){
		Group group = new Group();
		
		Cursor groupCursor = this.databaseAdapter.getGroupById(groupId);
		groupCursor.moveToFirst();
		group.setId(groupCursor.getString(groupCursor.getColumnIndex(DatabaseAdapter.GROUP_TABLE_COLUMN_ID)));
		group.setTitle(groupCursor.getString(groupCursor.getColumnIndex(DatabaseAdapter.GROUP_TABLE_COULMN_TITLE)));
		
		return group;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
		case R.id.activity_view_all_tasks_Menu_actionbar_Item_add_task:
			ApplicationNavigationHandler.addNewTask(this, this.databaseAdapter);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.view_all_tasks, menu);
		return true;
	}

}
